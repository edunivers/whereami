	/*
	Project: whereami
	Author: edunivers
	
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    */


package org.edunivers.whereami;

import java.util.ArrayList;
import java.util.List;

import org.edunivers.whereami.R;
import org.osmdroid.DefaultResourceProxyImpl;
import org.osmdroid.ResourceProxy;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapController;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.OverlayItem;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;


public class MainActivity extends Activity {

	public final static String EXTRA_MESSAGE = "org.fdroid.whereami.MESSAGE";

    private MapView mMapView;
	private MapController mMapController;

    private ArrayList<OverlayItem> overlayItemArray;
    private MyLocationNewOverlay myLocationOverlay;
     
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// Acquire a reference to the system Location Manager
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        
        if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
        	Toast.makeText(MainActivity.this, R.string.reading_gps,Toast.LENGTH_LONG).show();
        } else{
        	AlertDialog.Builder builder = new AlertDialog.Builder(this);  
            builder.setMessage(R.string.enable_gps)  
                   .setCancelable(false)  
                   .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {  
                       public void onClick(DialogInterface dialog, int id) {  
                           Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);  
                       startActivityForResult(intent, 1);  
                       }  
                   })  
                   .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {  
                       public void onClick(DialogInterface dialog, int id) {  
                           MainActivity.this.finish();  
                       }  
                   }).show();  
        }
        // Define a listener that responds to location updates
        LocationListener locationListener = new LocationListener() {
        	    
        	public void onLocationChanged(Location location) {
        		// Called when a new location is found by the network location provider.
        		makeUseOfNewLocation(location);
        	}
        	
        	public void onStatusChanged(String provider, int status, Bundle extras) {}
        	public void onProviderEnabled(String provider) {}
        	public void onProviderDisabled(String provider) {}
        };

        // Register the listener with the Location Manager to receive location updates
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
                
        mMapView = (MapView) findViewById(R.id.openmapview);
        mMapView.setTileSource(TileSourceFactory.MAPNIK);
        mMapView.setBuiltInZoomControls(true);
        mMapView.setMultiTouchControls(true);
        mMapController = (MapController) mMapView.getController();
        mMapController.setZoom(5);
        	
        GeoPoint gPt = new GeoPoint(41387018, 2170043);
        //GeoPoint gPt = new GeoPoint(location.getLatitude()* 1e6, location.getLongitude()* 1e6);
        	
        //Centre map near to Barcelona, Spain
        mMapController.setCenter(gPt);
        	        	
        //Create Overlay
        overlayItemArray = new ArrayList<OverlayItem>();
            
        DefaultResourceProxyImpl defaultResourceProxyImpl = new DefaultResourceProxyImpl(this);
        MyItemizedIconOverlay myItemizedIconOverlay = new MyItemizedIconOverlay(overlayItemArray, null, defaultResourceProxyImpl);
        mMapView.getOverlays().add(myItemizedIconOverlay);
        
        myLocationOverlay = new MyLocationNewOverlay(this, mMapView);
        mMapView.getOverlays().add(myLocationOverlay);
        mMapView.postInvalidate();
            
    }
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu items for use in the action bar
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    case R.id.action_about:
	    	AlertDialog.Builder builder = new AlertDialog.Builder(this);  
	        builder.setMessage(R.string.license)  
	                   .setCancelable(true)  
	                   .show();  
	        
	        return true;
	    //  ... other item selection handling
	    default:
	        return super.onOptionsItemSelected(item);
	    }
	}
	
    @Override
    protected void onResume() {
    	super.onResume();
    	myLocationOverlay.enableMyLocation();
    }

    @Override
    protected void onPause() {
    	super.onPause();
    	myLocationOverlay.disableMyLocation();
    }
    		
	private void makeUseOfNewLocation(Location location) {
    	GeoPoint gPt = new GeoPoint(location.getLatitude(), location.getLongitude());
    	
    	//Centre map to my position
    	mMapController.setCenter(gPt);
    	setOverlayLocation(location);
    	mMapController.setZoom(16);
        
        mMapView.invalidate();
    	
        String longitude = Double.toString(((double)location.getLongitude()));
        String latitude = Double.toString(((double)location.getLatitude()));
        
        Toast.makeText(MainActivity.this, "Lon: "+longitude +"\n"+ "Lat: "+latitude,Toast.LENGTH_SHORT).show();
        
	}
    
    private void setOverlayLocation(Location overlaylocation){
    	  GeoPoint overlocGeoPoint = new GeoPoint(overlaylocation);
    	  
    	  overlayItemArray.clear();
    	  
    	  OverlayItem newMyLocationItem = new OverlayItem("My Location", "My Location", overlocGeoPoint);
    	  overlayItemArray.add(newMyLocationItem);   
    }
    
   private class MyItemizedIconOverlay extends ItemizedIconOverlay<OverlayItem>{

    	  public MyItemizedIconOverlay(
    	    List<OverlayItem> pList,
    	    org.osmdroid.views.overlay.ItemizedIconOverlay.OnItemGestureListener<OverlayItem> pOnItemGestureListener,
    	    ResourceProxy pResourceProxy) {
    		  super(pList, pOnItemGestureListener, pResourceProxy);
    	  }
   }
    
}

